# SusyPlotting

Setting Up:
Install numpy
export PYTHONPATH=$PYTHONPATH:$PATH/CPP/lib


my_configurations/my_regions.py     - sets the regions, they all need unique names
my_configurations/my_vars.py        - lists the variables that you want to plot
my_configurations/my_inputs.py      - select your input files and add weights, lumi and rel_unc etc 
my_configurations/my_syst.py        - selects systematics 


Running:
./run.py -c my_configurations/my_regions.py my_configurations/my_vars.py my_configurations/my_inputs.py my_configurations/my_syst.py -vv -j 2 --nobuffer -nmo
(You can create different configuration files, as long as you list the four specific ones you are using.)

All the following arguments are optional and can be run without:
-vv         - prints the debug log to screen
-j 2        - sets how many nodes it will run on (feynman has 24 nodes, but we're not supposed to use too many at once!)
--nobuffer  - including this will generate the plots from scratch and save the histo info in the output/ folder.  Running 
              without this, will buffer the previously saved histo info which is great if you only want to change plot 
              cosmetics.  If you change the binning, you'll need to re-run with --nobuffer selected.
-nmo        - selecting N-1 plots instead of full kinematic.  Including this will change the ratio to Zn curves.  Omit this for ratio.

When running for N-1 Plots:
  You can add arrows showing variable cuts on both the kinematic plots and N-1 plot. In the region definitions you can include the 
  N-1 variables in the "n_mo_cuts" list. You can list as many variables as you like within the "n_mo_cuts" list, it will still make 
  all the cuts except the one variable that is being plotted - you can also reverse the Zn curve by setting reverse_zn=True in my_vars.py 
  (see examples)

  You can set the N-1 arrow height, length and whether you want it to be a single or double headed arrow in my_regions.py, for example:
  Format is: ('Variable', lower_cut, upper_cut, lower_double_arrow, upper_double_arrow, arrow_height,arrow_length)
  lower_double_arrow and upper_double_arrow are either true or false, depending on whether you want it to be a double headed arrow or not
  Example: n_mo_cuts=[  ('LepPt[2]', 50, 80, 1,1,15,5),  ]
  Which will set the lowercut on 3rd leading lepton Pt as 50GeV, the uppercut as 80GeV, both the lower and upper cuts will have double
  headed arrows, which will be 15 events high with arrows of 5GeV wide.

Cosmetics:
  You can change most of the cosmetics of the plots (such as legend position, scaling, displaying yields etc) in the scripts below (the first is 
  when you are running the ratio plot, the second is when you run the Zn curve.)
  > plotting/stack_plotting.py
  > plotting/n_minus_one_plotting.py

Changing text size and labels can be done in:
  > plotting/utils.py
