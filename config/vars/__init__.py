from collections import OrderedDict

_all = OrderedDict()


def get(*name):
    if len(name) == 0:
        return None
    if len(name) == 1:
        return _all[name[0]] if name[0] in _all else None

    return [_all[r] for r in name if name in _all]


def get_all():
    """Returns a list of all variables."""
    return _all.values()


class variable:
    """Class to store information on variables that are plotted."""

    def __init__(self, name, branch=None, unit=None, label=None, bins=None,
                 reverse_zn=False):
        """Creates a variable with a given name. Label and unit can be
        specified for the plotting."""
        self.name = name
        self.branch = branch if branch else name
        self.unit = unit
        self.bins = bins
        self.reverse_zn = reverse_zn
        self.label = name if label is None else label
        _all[name] = self

    def get_full_xlabel(self):
        lbl = r'{}'.format(self.label)
        if self.unit:
            lbl += r' [{}]'.format(self.unit)

        return lbl

    def __repr__(self):
        """Return a string uniquely identifying this variable to and its
        properties which are relevant for the actual filling of histogram:
        varname, branch name and the binning!"""
        return '{}:{}:{}'.format(self.name, self.branch, ','.join([str(x) for x in self.bins]))
