import argparse


def create_parser():
    """Argument parser for the main script
    """
    parser = argparse.ArgumentParser(
        description='Makes stack plots for all specified variables, regions'
        'systematics and inputs'
    )
    parser.add_argument(
        '-c', '--cfiles', required=True, nargs='+',
        help='Config files to be executed which define things'
    )
    parser.add_argument(
        '-n', '--nobuffer', default=False, action='store_true',
        help='Will not load anything from the buffer.'
    )
    parser.add_argument(
        '-a', '--autobuffer', default=False, action='store_true',
        help='Will automatically decide if buffered content is valid.'
    )
    parser.add_argument(
        '-s', '--autosystbuffer', default=False, action='store_true',
        help='Will automatically correct buffered systs if necessary.'
    )
    parser.add_argument(
        '-v', '--verbose', action='count', default=0,
        help='Set logging level: v - WARN, vv - INFO, vvv - DEBUG'
    )
    parser.add_argument(
        '-j', '--jobs', default=1, type=int,
        help='Number of parallel processes to run for the main loop'
    )
    parser.add_argument(
        '-nmo', '--nmo', default=False, action='store_true',
        help='N-1 disbtributions.'
    )

    return parser.parse_args()
