from config.vars import variable


#VR vars
#variable(name='mll3lZmin', unit='GeV', label='m_{SFOS}^{min}', bins=(10, 1.2, 201.2))#,reverse_zn='true')
#variable(name='ST', branch='eT_miss+LepPt[0]+LepPt[1]+LepPt[2]',unit='GeV', label='S_{T}', bins=(40, 0, 1000))#,reverse_zn='true')
#variable(name='LT', branch='sqrt((LepPt_x[0]+LepPt_x[1]+LepPt_x[2])*(LepPt_x[0]+LepPt_x[1]+LepPt_x[2])+(LepPt_y[0]+LepPt_y[1]+LepPt_y[2])*(LepPt_y[0]+LepPt_y[1]+LepPt_y[2]))',unit='GeV', label='L_{T}', bins=(25, 0, 500))
#variable(name='HTLep', branch='LepPt[0]+LepPt[1]+LepPt[2]',unit='GeV', label='H_{T}^{Lep}', bins=(24, 50, 650))
#variable(name='mTWZ', unit='GeV', label='m_{T}^{Zmass}', bins=(10, 0, 200))#,reverse_zn='true')ns=(20, 0, 300))
#variable(name='mTWZ', unit='GeV', label='m_{T}', bins=(14, 0, 350))
#variable(name='eT_miss', unit='GeV', label='E_{T}^{miss}', bins=(20, 50, 550))
#variable(name='mTWZ', unit='GeV', label='m_{T}', bins=(8, 0, 200))#,for Wh only
#variable(name='eT_miss', unit='GeV', label='E_{T}^{miss}', bins=(8, 50, 250))# for Wh only
variable(name='MET', branch='eT_miss', unit='GeV', label='E_{T}^{miss}', bins=(15, 50, 350))
#variable(name='LepPt[0]', unit='GeV', label='p_{T}^{l_{1}}', bins=(15,25, 325))#,reverse_zn='true')
#variable(name='LepPt[1]', unit='GeV', label='p_{T}^{l_{2}}', bins=(13,20, 280))#,reverse_zn='true')
#variable(name='LepPt[2]', unit='GeV', label='p_{T}^{l_{3}}', bins=(8,10, 170))
#variable(name='LepEta[0]', unit='', label='|#eta|(l_{1})', bins=(25,-2.5,2.5))
#variable(name='LepEta[1]', unit='', label='|#eta|(l_{2})', bins=(25,-2.5,2.5))
#variable(name='LepEta[2]', unit='', label='|#eta|(l_{3})', bins=(25,-2.5,2.5))
#variable(name='ht', unit='GeV', label='H_{T}', bins=(25,20,520))
#variable(name='nJets', unit='', label='nJets', bins=(6, 0, 6))
#variable(name='num_bjets', unit='', label='nbJets', bins=(3, 0, 3))
#variable(name='JetPt', unit='GeV', label='p_{T} (Jets)', bins=(12,20, 320))#,reverse_zn='true')
#variable(name='mll3lZ', unit='GeV', label='m_{ll}', bins=(50, 0, 200))#,reverse_zn='true')
#variable(name='mll3lZmin', unit='GeV', label='m_{SFOS}^{min}', bins=(50, 0, 200))#,reverse_zn='true')
#variable(name='JetEta', unit='', label='|#eta|(jet)', bins=(25,-2.5,2.5))#,reverse_zn='true')
#variable(name='mlll', unit='GeV', label='m_{3l}', bins=(20, 0, 300))
#variable(name='mlll_mz', branch ='fabs(mlll-91.2)', unit='GeV', label='|m_{3l}-m_{Z}|', bins=(20, 0, 300))
#variable(name='met_Sig', unit='', label='Significance', bins=(40, 0, 20))
#variable(name='dPhiWlep', unit='', label='#delta#phi(W,Lep)', bins=(32,0,3.2))
#variable(name='dPhiWlepMin', unit='', label='#delta#phi(W,Lep)', bins=(32,0,3.2))
#variable(name='dPhiZeT', unit='', label='#delta#phi(Z,E_{T}^{miss})', bins=(32,0,3.2))
#variable(name='ddPhiZeTmin', unit='', label='#delta#phi(Z,E_{T}^{miss})', bins=(32,0,3.2))
#variable(name='dPhilepeT', unit='', label='#delta#phi(Lep,E_{T}^{miss})', bins=(32,0,3.2))
#variable(name='dPhilepeTmin', unit='', label='#delta#phi(Lep,E_{T}^{miss})', bins=(32,0,3.2))
#variable(name='dPhiZW', unit='', label='#delta#phi(Z,W)', bins=(32,0,3.2))
