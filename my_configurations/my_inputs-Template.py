from config.inputs import input, merge
from math import sqrt
import ROOT
import numpy as np

lumi = 36100.

# **For histo buffering reasons, if the root file name changes, the name label
# must also change, otherwise it will throw a mismatch error**


# Define directory paths
basedir = '/lustre/scratch/epp/atlas/ntuples/SUSY2/bkg_filepath/'
sigdir  = '/lustre/scratch/epp/atlas/ntuples/SUSY2/sig_filepath/'
datadir = '/lustre/scratch/epp/atlas/ntuples/SUSY2/data_filepath/'

# Define weights
basesel ='EventWeight*XSecWeight*elecSF*muonSF*pileupweight*jvtSF'
fakesel ='eventweight'
sigsel  ='EventWeight*m_3lepweight*elecSF*muonSF*pileupweight*jvtSF'
datasel ='EventWeight'


# Include data
input(name='Data',  files=datadir+'data.root',      treename='HFntuple',    color=ROOT.kBlack,      scale=1.,   weights=datasel,  label='Data', isData=True)

# Include fake ntuple
# force_positive prevents negative fakes substracting from the total SM
input(name='Fakes', files=basedir+'Fake.root',      treename='Fakes',       color=ROOT.kOrange-2,   scale=36.1, weights=fakesel,  label='Reducible', force_positive=True)

# Include backgrounds
# rel_unc allows the addition of uncertainties, such as theory uncertainties,
# that can be assigned differently to the different SRs and VRs
input(name='Higgs', files=basedir+'Higgs.root',     treename='HFntuple',    color=ROOT.kViolet-9,   scale=lumi, weights=basesel,    label='Higgs',      rel_unc={'SR3-a':(0.068,0.047),'SR3-b':(0.016,0.017),'SR3-c':(0.13,0.126),'VR3-a':(0.041,0.079),'VR3-b':(0.074,0.132)})
input(name='ttV',   files=basedir+'ttV.root',       treename='HFntuple',    color=ROOT.kCyan-1,     scale=lumi, weights=basesel,    label='t#bar{t}V',  rel_unc={'SR3-a':(0.068,0.047),'SR3-b':(0.016,0.017),'SR3-c':(0.13,0.126),'VR3-a':(0.041,0.079),'VR3-b':(0.074,0.132)})
input(name='VVV',   files=basedir+'Tribosons.root', treename='HFntuple',    color=ROOT.kMagenta-10, scale=lumi, weights=basesel,    label='VVV',        rel_unc={'SR3-a':(0.068,0.047),'SR3-b':(0.016,0.017),'SR3-c':(0.13,0.126),'VR3-a':(0.041,0.079),'VR3-b':(0.074,0.132)})

# Include merged ntuples
# Keep same color and label
# Can also add SF in weights
# NB. comma after first merged ntuple
merge(
    input(name='VV', files=basedir+'SherpaWZ.root', treename='HFntuple',    color=ROOT.kAzure+1, scale=lumi, weights=basesel+'*0.945',  label='VV', rel_unc={'SR3-a':(0.068,0.047),'SR3-b':(0.016,0.017),'SR3-c':(0.13,0.126),'VR3-a':(0.041,0.079),'VR3-b':(0.074,0.132)}),
    input(name='ZZ', files=basedir+'SherpaZZ.root', treename='HFntuple',    color=ROOT.kAzure+1, scale=lumi, weights=basesel,           label='VV', rel_unc={'SR3-a':(0.068,0.047),'SR3-b':(0.016,0.017),'SR3-c':(0.13,0.126),'VR3-a':(0.041,0.079),'VR3-b':(0.074,0.132)})
)

# Include signals
input(name='Signala', files=sigdir+'MGPy8EG_A14N23LO_C1N2_Slep_1100_0_2L5.root',    treename='HFntuple', color=ROOT.kRed-4, linestyle=5,    scale=lumi, weights=sigsel, label='(1100,0) GeV',   isSignal=True)
input(name='Signalb', files=sigdir+'MGPy8EG_A14N23LO_C1N2_Slep_1000_600_2L5.root',  treename='HFntuple', color='#000080',   linestyle=5,    scale=lumi, weights=sigsel, label='(1000,600) GeV', isSignal=True)
input(name='Signalc', files=sigdir+'MGPy8EG_A14N23LO_C1N2_Slep_800_600_2L5.root',   treename='HFntuple', color='#32cd32',   linestyle=5,    scale=lumi, weights=sigsel, label='(800,600) GeV',  isSignal=True)
