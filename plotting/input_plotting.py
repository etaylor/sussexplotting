import ROOT
from config import syst, config
from utilities import helpers
from config import inputs
from plotting.utils import add_overflow_to_last_bin, get_integral
import logging as log
import pickle


class BufferMismatch(Exception):
    """Exception raised if mismatch between buffered histograms and provided
    config is detected"""
    pass


class BufferSystMismatch(Exception):
    """Exception raised if mismatch between buffered histograms and provided
    config is detected in terms of configured systematics"""
    pass


def get_the_tree(ifile, treename, sysname, isWeight=False):
    """Returns the Tree treename_suffix where the suffix is chosen as the
    name of the systematic. If the systematic is not found, the central
    tree is returned. If systematic is a weight, central tree is returned
    together with the name of the weight if it is a branch in the tree."""
    # Determine tree suffix based on whether we have a weighted systematic
    # or if it is a weight in the central tree
    central = syst.get_central()
    if isWeight:
        suffix = central.name
    else:
        suffix = sysname
    log.info('{}{}'.format(treename, suffix))
    tree = ifile.Get('{}{}'.format(treename, suffix))
    if not tree:
        log.info('Trying with underscore')
        tree = ifile.Get('{}_{}'.format(treename, suffix))

    # Now, two scenarios: first for non-weight systematics
    if isWeight is False:
        if not tree:
            log.debug('Did not find {}{}'.format(treename, sysname))
            ctr = syst.get_central()
            log.debug('Trying the central one: {}'.format(ctr.name))
            tree = ifile.Get('{}{}'.format(treename, ctr.name))
            if not tree:
                log.info('Trying with underscore')
                tree = ifile.Get('{}_{}'.format(treename, ctr.name))
        if not tree:
            log.error('Still no {}{}'.format(treename, sysname))
            log.error('Contribution is missing. This yields nonsense results.')
            raise inputs.MissingInputError
        # Return the tree. Second return value indicates additional weight
        # to be used. Does not make sense for non weight systematics, hence
        # None
        return tree, None
    else:
        if not tree:
            # log.error('Systematic is weighted but no central tree found.')
            log.error(
                'Systematic {} is weighted but no central tree found.'.format(
                    suffix))
            raise inputs.MissingInputError
        # No check if we have this systematic in the tree, else return None
        # for the additional weight to be used as a second argument. This
        # will happen if our tree has its systematics as an additional
        # tree somewhere else. Most notably in fake MM uncertainties vs the
        # rest
        # print list(tree.GetListOfAliases())
        branches = [b.GetName() for b in tree.GetListOfBranches()]
        if tree.GetListOfFriends():
            branches += [b.GetName() for ft in tree.GetListOfFriends() for b in ft.GetTree().GetListOfBranches()]

        # if sysname in tree.GetListOfBranches() or 'Fakes' in tree.GetName():
        if sysname in branches:
            log.info('Found {} as a branch'.format(sysname))
            return tree, sysname
        else:
            log.info('Not found {} as a branch'.format(sysname))
            return tree, None


def process_sys(region, var, ipt, sysname, isWeight=False, replaces=None,
                method=None):
    """Function opens the file and gets the tree and fills the histogram.
    If isWeight is false, test if a tree for this uncertainty exists.
    If it fails, the
    central tree is used. If isWeight is true, open the central tree and check
    if a corresponding branch exists. If not, do not modify the total weight
    and treat the tree like the central one. If replaces is defined, attempt
    to replace a previous weight by the modified weight."""
    # Make a unique name for this histogram to use the TTree::Draw
    # var>>hist_name feature to fill the histogram.
    ifile = ROOT.TFile.Open(ipt.files)
    hist_name = '{}_{}_{}_{}'.format(region.name, var.name, ipt.name,
                                     sysname)
    hist = ROOT.TH1F(hist_name, hist_name, *var.bins)

    # Some ugly histogram settings
    hist.SetTitle(var.get_full_xlabel())
    hist.SetTitle("Events / {:.0f} {}".format(
        var.bins[2]-var.bins[1]/var.bins[0], var.unit))
    hist.SetTitleOffset(0.5)
    hist.SetTitleSize(0.15)
    hist.SetMinimum(0.01)
    hist.SetMaximum(20.0)


    if not ipt.isData:
        color = helpers.get_color(ipt.color)
        hist.SetLineColor(color)
        if ipt.isSignal:
            hist.SetFillColor(ROOT.kWhite)
            hist.SetLineStyle(2)
        else:
            hist.SetFillColor(color)
            hist.SetLineStyle(1)
        hist.SetFillStyle(1001)
        hist.SetLineWidth(2)
        hist.SetMarkerSize(0)

    # TTree::Draw takes the weight to fill the histogram as its second argument.
    # Combine all the weights and scale and include the selection string which
    # either leaves the actual weight untouched or sets the weight to zero in
    # case the event fails the selection.
    sel_weight_string = '({})*({})*({})'.format(region.all_cuts(var.branch),
                                                ipt.weights,
                                                ipt.scale)

    if region.name in ipt.region_scales:
        sel_weight_string += '*({})'.format(
            ipt.region_scales[region.name])

    tree, add_weight = get_the_tree(ifile, ipt.treename, sysname, isWeight)
    tree.SetBranchStatus("*", 1)
    # If add_weight is returned, a systematic defined by an additional weight
    # is given, we have to add this as an additional weight.
    if add_weight:
        if replaces:
            if method == 'multiply':
                mod_string = '({}*{})'.format(replaces, add_weight)
            elif method == 'add':
                mod_string = '({}+{})'.format(replaces, add_weight)
            else:
                mod_string = add_weight
            sel_weight_string = sel_weight_string.replace(replaces, mod_string)
        else:
            sel_weight_string += '*({})'.format(add_weight)

    log.debug('Weight: {}'.format(sel_weight_string))
    log.debug('Filling: {}'.format(hist_name))
    log.debug('Number before TTree::Draw: {}'.format(get_integral(hist),
                                                     True, True))
    # FILLING OF THE HISTOGRAM:
    num = tree.Draw('{}>>{}'.format(var.branch, hist_name), sel_weight_string)
    log.debug('Read {} entries (unweighted)'.format(num))
    log.debug('Number after TTree::Draw: {}'.format(get_integral(hist,
                                                                 True, True)))
    # The histogram was added to the directory of the file. If the file
    # goes out of scope below, the histogram would be deleted. To prevent
    # this we associate it to the root dir.
    hist.SetDirectory(0)
    ifile.Close()

    # Move the content of the overflow bin into the last bin and clean the
    # overflow bin. This should preserve the total integrated value.
    add_overflow_to_last_bin(hist)
    if ipt.force_positive:
        nbins = hist.GetNbinsX()
        for ibin in range(0, nbins+1):
            binContent = hist.GetBinContent(ibin)
            if binContent < 0:
                hist.SetBinContent(ibin, 0)
    log.debug('Number after overflow correction: {}'.format(get_integral(hist,
                                                                         True,
                                                                         True)))

    return hist


def plot(var, region, input):
    """Function which adds a dictionary as attribute 'stored' to the input
    object. This disctionary is then filled with syst -> filled histogram
    for the specific input. Optionally, it buffers the filled histogram
    and loads them from disk instead of filling the histograms every time.

    ATTENTION: This is currently not multithreading safe and should only be
    used in multiprocessing.
    """
    outfile = config.output_prefix
    outfile += '/{}'.format(region.name)
    outfile += '/{}'.format(var.name)
    helpers.ensure_directory_exists(outfile)
    outfile += '/{}.p'.format(input.name)
    validfile = outfile.replace('.p', 'valid_strings.p')

    v_systs = {}

    if config.use_buffered_histograms:
        try:
            with open(outfile) as f:
                input.stored = pickle.load(f)
            # Try to open the buffer validation files
            try:
                with open(validfile) as f:
                    v_reg, v_var, v_ipt, v_systs = pickle.load(f)
            except:
                raise BufferMismatch('Failed opening buffer validation info!')
            # Compare the string representations to the actual in the buffer
            # to the one provided in the configuration
            if v_reg != str(region):
                raise BufferMismatch('Region mismatch: Expected {}, current configuration {}!'.format(v_reg, str(region)))  # NOQA
            if v_var != str(var):
                raise BufferMismatch('Variable mismatch: Expected {}, current configuration {}!'.format(v_var, str(var)))  # NOQA
            if v_ipt != str(input):
                raise BufferMismatch('Input mismatch: Expected {}, current configuration {}!'.format(v_ipt, str(input)))  # NOQA
            not_used = []
            for l_syst in input.stored:
                # Try here to be able to raise syst related problems
                try:
                    if syst.get(l_syst) is None:
                        raise BufferSystMismatch('Buffered {} not in current configuration'.format(l_syst))  # NOQA
                    elif v_systs[l_syst] != str(syst.get(l_syst)):
                        raise BufferSystMismatch('Systematic mismatch: Expected {}, current configuration {}!'.format(v_systs[l_syst], str(syst.get(l_syst))))  # NOQA
                except BufferSystMismatch as e:
                    if config.delete_throwing_systs:
                        log.warning('Removing systematic from buffer {}. Reason: {}'.format(l_syst, str(e)))  # NOQA
                        del input.stored[l_syst]
                        if l_syst in v_systs:
                            del v_systs[l_syst]
                        not_used.append(l_syst)
                    else:
                        # Need to reraise exception to be handled again by the
                        # main try
                        raise e
            if len(not_used) > 0:
                log.warning('For {} for {} {} not using buffered: {}'.format(
                    input.name, var.name, region.name,
                    ', '.join(l_syst)))
            log.info('Loaded {} for {} {}, Systematic: {}'.format(
                input.name, var.name, region.name,
                ', '.join(input.stored.keys())))
        # If buffer mismatch, skip the buffer if option set
        except BufferMismatch as e:
            if config.automatically_deactivate_buffer:
                log.warning('No buffered histograms for {} {} {}'.format(
                            input.name, var.name, region.name))
                log.warning('Reason: {}'.format(str(e)))
                input.stored = {}
            else:
                raise e
        except Exception as e:
            # If BufferSystMismatch was reraised prevent it from being ignored
            if type(e) == BufferSystMismatch:
                raise e
            log.info('No buffered histograms {} for {} {}'.format(
                     input.name, var.name, region.name))
            input.stored = {}
    else:
        input.stored = {}

    if input.isData or input.isSignal:
        systematics = [syst.get_central()]
    else:
        systematics = syst.get_all()
    for sys in systematics:
        # We compute the histogram for every input, if a systematic does not
        # exist for a specific input, the central tree is plotted. Though
        # this leads to duplication of trees (e.g. all SM backgrounds will
        # have the same tree for all MM fake systematics) this significantly
        # simplifies the computation of the actual systematic as it is
        # guaranteed that all contributions sum of to the total SM, even
        # if some of them are unchanged and just cancel when taking
        # the difference to central value.
        if sys.name not in input.stored:
            log.info('Making histograms {} {} {} {}'.format(
                region.name, var.name, input.name, sys.name))
            # If the systematic is up-down type, run both separately by
            # appending the suffix to the systematic.
            is_weight = sys.isWeight
            replaces = sys.replaces
            method = sys.method
            if sys.up_down:
                input.stored[sys.name] = (
                    process_sys(region, var, input,
                                sys.name + sys.up_down[0], is_weight, replaces,
                                method),
                    process_sys(region, var, input,
                                sys.name + sys.up_down[1], is_weight, replaces,
                                method))
            else:
                input.stored[sys.name] = process_sys(
                    region, var, input, sys.name, is_weight, replaces, method)

            v_systs[sys.name] = str(sys)

            log.info('Buffering histograms {} for {} {} {}'.format(
                    input.name, var.name, region.name, sys.name))
            # Write out the stored dictionary after every systematic so
            # progress is saved in case there are problems/crashes
            # such as broken pipes etc
            with open(outfile, 'w') as f:
                pickle.dump(input.stored, f)
            valid_info = (str(region), str(var), str(input), v_systs)
            with open(validfile, 'w') as f:
                pickle.dump(valid_info, f)


def make_sm_sum(var, region, allowed_inputs):
    """Make the sum off all standard model processes for all systematics.
    SM contributions are defined by the inputs which are not data or signal."""
    log.info('Making SM sums per uncertainty')
    sm_sums = {}
    all_inputs = allowed_inputs
    # only take the inputs which are not data or signal
    all_inputs = [i for i in all_inputs if not i.isData and not i.isSignal]
    for ipt in all_inputs:
        for sys, hist in ipt.stored.items():
            # If there is no entry for this systematic, clone the first one,
            # else add the content to the first one
            if sys not in sm_sums:
                # If the systematic is up-down type, have to work on both
                # types
                if syst.get(sys).up_down:
                    log.info('SM sums: Treating {} as up_down syst'.format(sys))
                    sm_sums[sys] = (hist[0].Clone('{}_SM_{}_{}_up'.format(region.name, var.name, sys)),
                                    hist[1].Clone('{}_SM_{}_{}_down'.format(region.name, var.name, sys)))
                else:
                    log.info('SM sums: Treating {} as central syst'.format(sys))
                    sm_sums[sys] = hist.Clone('{}_SM_sum_{}_{}'.format(
                        region.name, var.name, sys))
            else:
                if syst.get(sys).up_down:
                    log.debug('Adding {} to {}.'.format(
                        hist[0].GetName(), sm_sums[sys][0].GetName()))
                    sm_sums[sys][0].Add(hist[0])
                    log.debug('Adding {} to {}.'.format(
                        hist[1].GetName(), sm_sums[sys][1].GetName()))
                    sm_sums[sys][1].Add(hist[1])
                else:
                    log.debug('Adding {} to {}.'.format(hist.GetName(),
                                                        sm_sums[sys].GetName()))
                    sm_sums[sys].Add(hist)
    return sm_sums


def plot_scatter(var1, var2, region, ipt):
    """Function that plots a 2D histogram for the two variables given
    the provided region and input. Supports the n_mo_cuts settings for the
    two variables, ignoring the variable specific cuts when plotting"""
    # Make a unique name for this histogram to use the TTree::Draw
    # var>>hist_name feature to fill the histogram.
    ifile = ROOT.TFile.Open(ipt.files)
    hist_name = '{}_{}_{}_{}'.format(region.name, var1.name, var2.name,
                                     ipt.name)
    hist = ROOT.TH2F(hist_name, hist_name,
                     var1.bins[0], var1.bins[1], var1.bins[2],
                     var2.bins[0], var2.bins[1], var2.bins[2])

    # Some ugly histogram settings
    hist.GetXaxis().SetTitle(var1.get_full_xlabel())
    hist.GetYaxis().SetTitle(var2.get_full_xlabel())
    hist.GetXaxis().SetTitleOffset(1.2)
    hist.GetYaxis().SetTitleOffset(1.5)
    hist.GetZaxis().SetTitleOffset(1.5)
    hist.GetZaxis().SetTitle("Events / {:.1f}#times{:.1f} {}#times{}".format(
        (var1.bins[2]-var1.bins[1])/var1.bins[0],
        (var2.bins[2]-var2.bins[1])/var2.bins[0],
        var1.unit,
        var2.unit))
    hist.SetMinimum(0.5)
    hist.SetMaximum(20.0)

    # TTree::Draw takes the weight to fill the histogram as its second argument.
    # Combine all the weights and scale and include the selection string which
    # either leaves the actual weight untouched or sets the weight to zero in
    # case the event fails the selection.
    sysname = syst.get_central().name
    sel_weight_string = '({})*({})*({})'.format(
        region.all_cuts(var1.name, var2.name), ipt.weights, ipt.scale)
    tree, _ = get_the_tree(ifile, ipt.treename, sysname, False)
    # If add_weight is returned, a systematic defined by an additional weight
    # is given, we have to add this as an additional weight.

    log.debug('Weight: {}'.format(sel_weight_string))
    log.debug('Filling: {}'.format(hist_name))
    # FILLING OF THE HISTOGRAM:
    num = tree.Draw('{}:{}>>{}'.format(var1.name, var2.name, hist_name),
                    sel_weight_string)
    log.debug('Read {} entries (unweighted)'.format(num))
    # The histogram was added to the directory of the file. If the file
    # goes out of scope below, the histogram would be deleted. To prevent
    # this we associate it to the root dir.
    hist.SetDirectory(0)
    ifile.Close()

    return hist
