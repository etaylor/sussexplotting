from config import syst, config
import logging as log
from utilities.helpers import convert_hist_to_arrays
import numpy as np


def total_sm_uncertainty(sm_sums):
    log.info('Getting min max for each uncertainty')
    central = sm_sums[syst.get_central().name]
    _, stat_error = convert_hist_to_arrays(central, underflow=True)
    tot_min, tot_max = stat_error**2, stat_error**2
    for sys in syst.get_all():
        if sys.central:
            continue
        mi, ma = make_min_max_arrays(central, sm_sums[sys.name])
        log.info('{} --> down: {}, up: {}'.format(sys.name, mi, ma))
        tot_min += mi**2
        tot_max += ma**2
    return np.sqrt(tot_min), np.sqrt(tot_max)


def total_rel_unc(inputs, region):
    """Compute the relative uncertainty for the given inputs, using
    the region to determine the relative uncertainty for each input
    from the values stored in the input objects which are set
    when defining the different inputs.
    """
    log.info('Calculating total rel. uncertainty')
    central = syst.get_central().name
    total_unc_up, total_unc_down = 0., 0.
    for ipt in inputs:
        central_hist = ipt.stored[central]
        central_val, _ = convert_hist_to_arrays(central_hist, underflow=True)
        if region.name not in ipt.rel_unc:
            continue
        factor = ipt.rel_unc[region.name]

        if type(factor) == tuple:
            factor_up, factor_down = factor
        else:
            factor_up = factor
            factor_down = factor

        log.info('Input: {}, Region{} ----> relative uncertainty: {}'.format(ipt.name, region.name, factor))
        total_unc_up += (factor_up*central_val)**2.
        total_unc_down += (factor_down*central_val)**2.

    total_unc_up = np.sqrt(total_unc_up)
    total_unc_down = np.sqrt(total_unc_down)
    return total_unc_down, total_unc_up


def make_min_max_arrays(central, hist):
    """This function takes the histogram for the central value and the
    systematically varied histogram. If the histogram is provided as a tuple,
    it needs to be of the form (up, down).
    """
    # Convert hist to array, including the underflow bin so the bin index
    # matches
    central, _ = convert_hist_to_arrays(central, underflow=True)

    # First the easy case: No up-down type uncertainty
    if not isinstance(hist, tuple):
        hist, _ = convert_hist_to_arrays(hist, underflow=True)
        diff = central - hist
        min_d, max_d = -np.abs(diff), np.abs(diff)
    else:
        hist_up, _ = convert_hist_to_arrays(hist[0], underflow=True)
        hist_down, _ = convert_hist_to_arrays(hist[1], underflow=True)
        diff_up = central - hist_up
        diff_down = central - hist_down
        if config.use_conf_syst_convention:
            min_d, max_d = -np.abs(diff_down), np.abs(diff_up)
        else:
            min_d, max_d = make_min_max(diff_down, diff_up)
        # Even though the uncertainties are labelled up and down, this does
        # not necessarily reflect that the content in a bin increases for
        # the up and decreases for the down uncertainty.
        # Hence, compute the correct shift up and down in the bin value.
        if config.use_conf_syst_convention:
             min_d, max_d = -np.abs(diff_down), np.abs(diff_up)
        else:
            min_d, max_d = make_min_max(diff_down, diff_up)

    return min_d, max_d



@np.vectorize
def make_min_max(mi, ma):
    """Returns corrected min and max values which actually behave as such."""
    if ma > 0 and mi < 0:
        min_d = mi
        max_d = ma
    elif ma < 0 and mi > 0:
        min_d = ma
        max_d = mi
    elif ma >= 0 and mi >= 0 and ma >= mi:
        min_d = 0
        max_d = ma
    elif ma >= 0 and mi >= 0 and ma < mi:
        min_d = 0
        max_d = mi
    elif ma <= 0 and mi <= 0 and ma >= mi:
        min_d = mi
        max_d = 0
    elif ma <= 0 and mi <= 0 and ma < mi:
        min_d = ma
        max_d = 0
    else:
        min_d = 0.
        max_d = 0.
    return min_d, max_d
