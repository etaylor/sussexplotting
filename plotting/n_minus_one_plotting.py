import ROOT
from config import syst
from config import inputs
from utilities import helpers
from plotting.utils import add_overflow_to_last_bin, make_poisson_error_graph
from plotting.utils import plot_text, get_integral, rescale_limits, my_text
from plotting import utils
from plotting import plot_it
import numpy as np
import logging as log
from config import config
from plotting.stack_plotting import stack_it

# ROOT.TH1.AddDirectory(False)


def make_n_minus_one_plots(allowed_inputs, var, region, sm_sums):

    # Only get the sm inputs
    sm_inputs = [i for i in allowed_inputs if not i.isData and not i.isSignal]
    central_name = syst.get_central().name

    outfile = config.output_prefix
    outfile += '/n_minus_one/'#{}'.format(region.name)
    helpers.ensure_directory_exists(outfile)
    if var.reverse_zn:
        outfile += '/{}_rzn_{}.{{}}'.format(var.name, region.name)
    else:
        outfile += '/{}_{}.{{}}'.format(var.name, region.name)

    total_sm = sm_sums[central_name]

    N_sm = get_integral(total_sm, underflow=True)

    if N_sm < 0.0001:
        canvas_name = 'canvas_{}_{}'.format(var.name, region.name)
        canvas = ROOT.TCanvas(canvas_name, canvas_name, 800, 600)
        my_text(0.15, 0.82, 1, "Empty silly!", 20)
        for t in config.output_types:
            canvas.Print(outfile.format(t))
            print "Created: ", outfile.format(t)
        helpers.delete_tobject(canvas)
        return

    # Lots of boring canvas setup
    canvas_name = 'canvas_{}_{}'.format(var.name, region.name)
    canvas = ROOT.TCanvas(canvas_name, canvas_name, 800, 600)
    canvas.stupid_root = []
    canvas_1 = ROOT.TPad(canvas_name+'_1', canvas_name+'_1',
                         0.0, 0.30, 1.0, 1.0)
    canvas_1.SetBottomMargin(0.01)
    canvas_1.Draw()
    canvas.cd()
    canvas_2 = ROOT.TPad(canvas_name+"_2", canvas_name+"_2",
                         0.0, 0.0, 1.0, 0.30)
    canvas_2.SetTopMargin(0)
    canvas_2.SetBottomMargin(0.4)
    canvas_2.Draw()
    canvas_2.cd()

    canvas_1.cd()
    canvas_1.SetLogy(1)

    ##################################
    # Initialize the Legend
    ##################################

    legend = ROOT.TLegend(0.5, 0.4, 0.90, 0.9)
    legend.SetBorderSize(0)
    legend.SetFillStyle(0)
    legend.SetNColumns(2)
    legend.SetLineColor(0)
    legend.SetTextFont(42)
    legend.SetTextSize(0.042)

    ##################################
    # Stacking of the SM histograms
    ##################################
    stack = stack_it(var, region, sm_inputs)
    plot_it.plot_stack(stack, var)

    ##################################
    # Plotting the signal
    ##################################

    all_signals = [i for i in allowed_inputs if i.isSignal]
    for sig in all_signals:
        sig.stored[central_name].Draw('HISTSAME')

    ##################################
    # Total SM line
    ##################################

    plot_it.plot_total_sm(total_sm)

    ##################################
    # SM systematic uncertainty
    ##################################
    totsm, sq_errs = helpers.convert_hist_to_arrays(total_sm)
    zeros = np.zeros(len(totsm))
    errors = np.sqrt((0.3*totsm)**2 + sq_errs**2)
    ctr, wdh = [], []
    for ibin in range(1, total_sm.GetNbinsX()+1):
        ctr.append(total_sm.GetXaxis().GetBinCenter(ibin))
        wdh.append(total_sm.GetXaxis().GetBinWidth(ibin) / 2.)
    ctr = np.array(ctr)
    wdh = np.array(wdh)
    totalmc_SysErr= utils.make_error_graph(ctr, totsm, wdh, wdh,
                                           errors, errors)

    totalmc_SysErr.SetLineColor(ROOT.kBlack)
    totalmc_SysErr.SetFillColor(ROOT.kGray + 3)
    totalmc_SysErr.SetFillStyle(3004)
    totalmc_SysErr.SetLineStyle(1)
    totalmc_SysErr.SetLineWidth(3)
    totalmc_SysErr.SetMarkerSize(0)
    totalmc_SysErr.Draw("E2SAME")


    ##################################
    # Labels and legend
    ##################################

    # Same order for the legend as before.

#    legend.AddEntry(total_sm, 'Total SM = {:.2f}'.format(N_sm), "flp")
    legend.AddEntry(total_sm, 'Total SM ', "flp")

    for sm_ipt in sm_inputs:
        hist = sm_ipt.stored[central_name]
        Nbla = get_integral(hist, underflow=True)
#        legend.AddEntry(hist, sm_ipt.label + ' = {:.2f}'.format(Nbla), "f")
        legend.AddEntry(hist, sm_ipt.label, "f")

    for sig_ipt in all_signals:
        hist = sig_ipt.stored[central_name]
        Nbla = get_integral(hist, underflow=True)
#        legend.AddEntry(hist, sig_ipt.label + ' = {:.2f}'.format(Nbla), "l")
        legend.AddEntry(hist, sig_ipt.label, "l")

    legend.Draw()

    # Plot the energy, luminosity, ATLAS and region labels
    plot_text(region)

    # Can be used to rescale the y-axis
    rescale_limits((0.03, 1.6), pad=canvas_1, lo=True)

    ##################################
    # Making Arrows
    ##################################

    if var.branch in region.n_mo_cuts_dict:
        lower_bnd, upper_bnd, lower_double, upper_double, arrow_height, arrow_length = region.n_mo_cuts_dict[var.branch]
        #print "arrow_height: ", arrow_height
        if lower_bnd is not None and upper_bnd is None:
            canvas.stupid_root.append(make_arrow(lower_bnd, arrow_height, arrow_length, right=True))
        if upper_bnd is not None and lower_bnd is None:
            if lower_double is 0 and upper_double is 0:
                canvas.stupid_root.append(make_arrow(upper_bnd, arrow_height, arrow_length,right=False))
            if lower_double is 0 and upper_double is 1:
                canvas.stupid_root.append(make_arrow_double(upper_bnd, arrow_height, arrow_length,right=False))
            if lower_double is 1 and upper_double is 0:
                canvas.stupid_root.append(make_arrow(upper_bnd, arrow_height, arrow_length,right=False))
            if lower_double is 1 and upper_double is 1:
                canvas.stupid_root.append(make_arrow_double(upper_bnd, arrow_height, arrow_length,right=False))
        if lower_bnd is not None and upper_bnd is not None:
            if lower_double is 0 and upper_double is 0:
                canvas.stupid_root.append(make_arrow(lower_bnd, arrow_height, arrow_length, right=True))
                canvas.stupid_root.append(make_arrow(upper_bnd, arrow_height, arrow_length,right=False))
            if lower_double is 0 and upper_double is 1:
                canvas.stupid_root.append(make_arrow(lower_bnd, arrow_height, arrow_length, right=True))
                canvas.stupid_root.append(make_arrow_double(upper_bnd, arrow_height, arrow_length,right=False))
            if lower_double is 1 and upper_double is 0:
                canvas.stupid_root.append(make_arrow_double(lower_bnd, arrow_height, arrow_length, right=True))
                canvas.stupid_root.append(make_arrow(upper_bnd, arrow_height, arrow_length,right=False))
            if lower_double is 1 and upper_double is 1:
                canvas.stupid_root.append(make_arrow_double(lower_bnd, arrow_height, arrow_length, right=True))
                canvas.stupid_root.append(make_arrow_double(upper_bnd, arrow_height, arrow_length,right=False))


   ##################################
   # The zn plot
   ##################################
   # Change to the bottom pad
    canvas_2.cd()

    multigraph = ROOT.TMultiGraph()
    multigraph.stuff = []
    min_zn, max_zn = 100000, -1000000
    for sg_ipt in all_signals:
        log.debug('Adding {} to multigraph'.format(sg_ipt.name))
        zn_curve, mi, ma = zn_plot(var, total_sm, sg_ipt)
        multigraph.Add(zn_curve)
        multigraph.stuff.append(zn_curve)
        min_zn = mi if mi < min_zn else min_zn
        max_zn = ma if ma > max_zn else max_zn

    log.debug('Setting {} {} as min/max for zn_plot'.format(min_zn, max_zn))
#    multigraph.SetMinimum(min_zn)
    multigraph.SetMinimum(-0.05)
    multigraph.SetMaximum(max_zn)
    multigraph.Draw('AL')
    multigraph.GetXaxis().SetLimits(var.bins[1], var.bins[2])
    multigraph.GetXaxis().SetTitleOffset(4.)
    multigraph.GetXaxis().SetTitle(var.get_full_xlabel())

    multigraph.GetYaxis().SetTitle('Zn 15%')
    multigraph.GetYaxis().SetNdivisions(503)
    multigraph.GetYaxis().SetTitleOffset(1.1)

    ROOT.gPad.Update()

    if var.branch in region.n_mo_cuts_dict:
        lower_bnd, upper_bnd, _, _, _, _ = region.n_mo_cuts_dict[var.branch]
        if lower_bnd is not None:
            canvas.stupid_root.append(make_zn_line(lower_bnd))
        if upper_bnd is not None:
            canvas.stupid_root.append(make_zn_line(upper_bnd))

    MyZeroLine = ROOT.TLine(ROOT.gPad.GetUxmin(), 0, ROOT.gPad.GetUxmax(), 0)
    MyZeroLine.SetLineWidth(2)
    MyZeroLine.SetLineColor(ROOT.kBlack)
    MyZeroLine.Draw()



    ##################################
    # Saving the plot
    ##################################

    for t in config.output_types:
        canvas.Print(outfile.format(t))
        print "Created: ", outfile.format(t)
    helpers.delete_tobject(canvas)
    helpers.delete_tobject(legend)
    helpers.delete_tobject(stack)
    helpers.delete_tobject(multigraph)


@np.vectorize
def zn_calc(bY, sY):
    return ROOT.RooStats.NumberCountingUtils.BinomialExpZ(sY, bY, 0.15)


def zn_plot(var, total_sm, sg_ipt):
    total_sig = sg_ipt.stored[syst.get_central().name]
    ctr = []
    wdh = []
    for ibin in range(1, total_sm.GetNbinsX()+1):
        ctr.append(total_sm.GetXaxis().GetBinCenter(ibin))
        wdh.append(total_sm.GetXaxis().GetBinWidth(ibin) / 2.)
    ctr = np.array(ctr)
    wdh = np.array(wdh)
    sm, _ = helpers.convert_hist_to_arrays(total_sm)
    sg, _ = helpers.convert_hist_to_arrays(total_sig)
    sm_int = []
    sg_int = []
    if var.reverse_zn:
        for i in range(len(sm)):
            sm_int.append(sum(sm[:i]))
            sg_int.append(sum(sg[:i]))
    else:
        for i in range(len(sm)):
            sm_int.append(sum(sm[i:]))
            sg_int.append(sum(sg[i:]))
    zeros = np.zeros(len(sm_int))

    #These lines do not exist in previous version###########
    sm_int = np.array(sm_int)
    sg_int = np.array(sg_int)
    mask = (sm_int > 0)  # Only SM > 0
    sm_int, sg_int, ctr = sm_int[mask], sg_int[mask], ctr[mask]
    #########################################################


    zn_vals = zn_calc(sm_int, sg_int)
    zn_curve = utils.make_error_graph(ctr, zn_vals, zeros, zeros, zeros, zeros)

    zn_curve.SetLineColor(helpers.get_color(sg_ipt.color))
    zn_curve.SetLineWidth(2)
    # Set the value of the range from 0 to 2. Use crude numbers to prevent
    # ROOT from placing an axis label for 0 and 2 at the edges of the y axis
    # which would collide with the top plot.
    min_zn = min(zn_vals)
    min_zn = 0.8*min_zn if min_zn > 0. else 1.2*min_zn
    max_zn = max(zn_vals)
    max_zn = 1.2*max_zn if max_zn > 0. else 0.8*max_zn
    return zn_curve, min_zn, max_zn

def make_arrow(position, length=15,height=5, right=True):
    MyCutLine = ROOT.TLine(position,0,position, length)
    MyCutLine.SetLineWidth(2)
    MyCutLine.SetLineColor(ROOT.kBlack)
    MyCutLine.Draw()
    if position is 0:
        MyArrowRight = ROOT.TArrow(0,0,0,0)
        MyArrowLeft = ROOT.TArrow(0,0,0,0)
    else:
        if right:
            MyArrow = ROOT.TArrow(position,length,position+height,length)
        else:
            MyArrow = ROOT.TArrow(position,length,position-height,length)
        MyArrow.SetLineWidth(2)
        MyArrow.SetArrowSize(0.02)
        MyArrow.SetLineColor(ROOT.kBlack)
        MyArrow.Draw()
    return MyCutLine, MyArrow

def make_arrow_double(position, length=15,height=5, right=True):
    MyCutLine = ROOT.TLine(position,0,position, length)
    MyCutLine.SetLineWidth(3)
    MyCutLine.SetLineColor(ROOT.kBlack)
    MyCutLine.Draw()
    if position is 0:
        MyArrowRight = ROOT.TArrow(0,0,0,0)
        MyArrowLeft = ROOT.TArrow(0,0,0,0)
    else:
        MyArrowRight = ROOT.TArrow(position,length,position+height,length)
        MyArrowLeft = ROOT.TArrow(position,length,position-height,length)
        MyArrowRight.SetLineWidth(2)
        MyArrowRight.SetArrowSize(0.02)
        MyArrowRight.SetLineColor(ROOT.kBlack)
        MyArrowRight.Draw()
        MyArrowLeft.SetLineWidth(2)
        MyArrowLeft.SetArrowSize(0.02)
        MyArrowLeft.SetLineColor(ROOT.kBlack)
        MyArrowLeft.Draw()
    return MyCutLine, MyArrowRight, MyArrowLeft

def make_zn_line(position):
    MyCutLine = ROOT.TLine(position, ROOT.gPad.GetUymin(), position,
                           ROOT.gPad.GetUxmax())
    MyCutLine.SetLineWidth(2)
    MyCutLine.SetLineColor(ROOT.kBlack)
    MyCutLine.Draw()
    return MyCutLine
