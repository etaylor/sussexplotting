import ROOT
from config import syst
from config import inputs
from utilities import helpers
from plotting.utils import add_overflow_to_last_bin, make_poisson_error_graph
from plotting.utils import plot_text, get_integral, rescale_limits, my_text
from plotting import utils
from plotting import plot_it
import numpy as np
import logging as log
from config import config
from utilities.helpers import ensure_directory_exists
import os

# ROOT.TH1.AddDirectory(False)


def stack_it(var, region, considered_inputs):
    """Loops over all inputs and gets the stored central value distribution
    and adds all of them to a TH1Stack. Returns that stack"""
    central_name = syst.get_central().name
    stack = None  # Is set by the first input in the loop.

    # Get a list of the histograms to be stacked and sort them if desired
    hists = [h.stored[central_name] for h in considered_inputs]
#    if config.sort_stack_by_integral:
        #hists = sorted(hists, key=lambda x: get_integral(x))
    hists = sorted(hists, key=get_integral)
    for hist in hists:
        if stack is None:
            stack = ROOT.THStack('stack_{}'.format(var.name, region.name), '')
            stack.Add(hist)
        else:
            stack.Add(hist)

    return stack


def make_final_plots(allowed_inputs, var, region, sm_sums, min_max_total):
    # Optional root file to store the histograms in case we need them later.
    # Only done if config.histogram_root_folder is specified

    # Only get the sm inputs
    sm_inputs = [i for i in allowed_inputs if not i.isData and not i.isSignal]
    central_name = syst.get_central().name

    outfile = config.output_prefix
    outfile += '/{}'.format(region.name)
    helpers.ensure_directory_exists(outfile)
    outfile += '/{}_{}.{{}}'.format(var.name, region.name)

    total_sm = sm_sums[central_name]

    N_sm = get_integral(total_sm, underflow=True)
    if N_sm < 0.0001:
        canvas_name = 'canvas_{}_{}'.format(var.name, region.name)
        canvas = ROOT.TCanvas(canvas_name, canvas_name, 800, 600)
        my_text(0.15, 0.82, 1, "Empty silly!", 20)
        for t in config.output_types:
            canvas.Print(outfile.format(t))
            print "Created: ", outfile.format(t)
        helpers.delete_tobject(canvas)
        return

    rootfile = None
    if config.histogram_root_folder:
        _fname = os.path.join(config.output_prefix,
                              config.histogram_root_folder)
        ensure_directory_exists(_fname)
        rootfile = ROOT.TFile(os.path.join(
            _fname,'{}_{}.root'.format(var.name, region.name))
            , 'RECREATE')

    # Lots of boring canvas setup
    canvas_name = 'canvas_{}_{}'.format(var.name, region.name)
    canvas = ROOT.TCanvas(canvas_name, canvas_name, 800, 600)
    canvas.stupid_root = []
    canvas_1 = ROOT.TPad(canvas_name+'_1', canvas_name+'_1',
                         0.0, 0.30, 1.0, 1.0)
    canvas_1.SetBottomMargin(0.01)
    canvas_1.SetTopMargin(0.03)
    canvas_1.SetBorderMode(0)
    canvas_1.Draw()
    canvas.cd()
    canvas_2 = ROOT.TPad(canvas_name+"_2", canvas_name+"_2",
                         0.0, 0.0, 1.0, 0.30)
    canvas_2.SetTopMargin(0)
    canvas_2.SetBottomMargin(0.4)
    canvas_2.Draw()
    canvas_2.cd()

    canvas_1.cd()
    canvas_1.SetLogy(1)

    ##################################
    # Initialize the Legend
    ##################################

    legend = ROOT.TLegend(0.5, 0.5, 0.9, 0.90)
    legend.SetBorderSize(0)
    legend.SetFillStyle(0)
    legend.SetLineColor(0)
    legend.SetNColumns(2)
    legend.SetTextFont(42)
    legend.SetTextSize(0.06)

    ##################################
    # Stacking of the SM histograms
    ##################################
    stack = stack_it(var, region, sm_inputs)
    plot_it.plot_stack(stack, var)

    ##################################
    # Plotting the signal
    ##################################

    all_signals = [i for i in allowed_inputs if i.isSignal]
    for sig in all_signals:
        color = helpers.get_color(sig.color)
        sig_hist = sig.stored[central_name]
        if rootfile:
            rootfile.WriteTObject(sig_hist)
        sig_hist.SetLineStyle(sig.linestyle)
        sig_hist.SetLineColor(color)
        sig_hist.SetLineWidth(4)
        sig_hist.Draw('HISTSAME')

    ##################################
    # Total SM line
    ##################################

    plot_it.plot_total_sm(total_sm)

    ##################################
    # SM systematic uncertainty
    ##################################

    totalmc_SysErr = helpers.createGraphAsymmErrors(
        total_sm, *min_max_total, fl_name=outfile.format('txt'))

    if rootfile:
        rootfile.WriteTObject(totalmc_SysErr, "Background")
    totalmc_SysErr.SetLineColor(ROOT.kBlack)
    totalmc_SysErr.SetFillColor(ROOT.kGray + 3)
    totalmc_SysErr.SetFillStyle(3004)
    totalmc_SysErr.SetLineStyle(1)
    totalmc_SysErr.SetLineWidth(3)
    totalmc_SysErr.SetMarkerSize(0)
    totalmc_SysErr.Draw("E2zSAME")


    ##################################
    # Making Arrows
    ##################################

    if var.branch in region.n_mo_cuts_dict:
        lower_bnd, upper_bnd, lower_double, upper_double, arrow_height, arrow_length = region.n_mo_cuts_dict[var.branch]
        #print "arrow_height: ", arrow_height
        if lower_bnd is not None and upper_bnd is None:
            canvas.stupid_root.append(make_arrow(lower_bnd, arrow_height, arrow_length, right=True))
        if upper_bnd is not None and lower_bnd is None:
            if lower_double is 0 and upper_double is 0:
                canvas.stupid_root.append(make_arrow(upper_bnd, arrow_height, arrow_length,right=False))
            if lower_double is 0 and upper_double is 1:
                canvas.stupid_root.append(make_arrow_double(upper_bnd, arrow_height, arrow_length,right=False))
            if lower_double is 1 and upper_double is 0:
                canvas.stupid_root.append(make_arrow(upper_bnd, arrow_height, arrow_length,right=False))
            if lower_double is 1 and upper_double is 1:
                canvas.stupid_root.append(make_arrow_double(upper_bnd, arrow_height, arrow_length,right=False))
        if lower_bnd is not None and upper_bnd is not None:
            if lower_double is 0 and upper_double is 0:
                canvas.stupid_root.append(make_arrow(lower_bnd, arrow_height, arrow_length, right=True))
                canvas.stupid_root.append(make_arrow(upper_bnd, arrow_height, arrow_length,right=False))
            if lower_double is 0 and upper_double is 1:
                canvas.stupid_root.append(make_arrow(lower_bnd, arrow_height, arrow_length, right=True))
                canvas.stupid_root.append(make_arrow_double(upper_bnd, arrow_height, arrow_length,right=False))
            if lower_double is 1 and upper_double is 0:
                canvas.stupid_root.append(make_arrow_double(lower_bnd, arrow_height, arrow_length, right=True))
                canvas.stupid_root.append(make_arrow(upper_bnd, arrow_height, arrow_length,right=False))
            if lower_double is 1 and upper_double is 1:
                canvas.stupid_root.append(make_arrow_double(lower_bnd, arrow_height, arrow_length, right=True))
                canvas.stupid_root.append(make_arrow_double(upper_bnd, arrow_height, arrow_length,right=False))
                #To manually set different arrow heights:
                #canvas.stupid_root.append(make_arrow_double(lower_bnd, 10, arrow_length, right=True))
                #canvas.stupid_root.append(make_arrow_double(upper_bnd, 1.8, arrow_length,right=False))

    ##################################
    # Data
    ##################################

    data = [d for d in allowed_inputs if d.isData]
    if region.blinded is False and len(data) == 1:
        # Only support one data file for now and store the input and get
        # the histogram
        data = data[0]
        data_hist = data.stored[central_name]
        data_hist.SetMarkerSize(1.2)
        data_hist.SetLineWidth(4)
        data_graph = make_poisson_error_graph(data_hist)
        data_graph.Draw("epz")

        if rootfile:
            rootfile.WriteTObject(data_graph, "Data")
        N_data = get_integral(data_hist, underflow=True)
    else:
        data = None
        data_hist = None
        data_graph = None

    ##################################
    # Labels and legend
    ##################################

    # Same order for the legend as before.
    if data:
        #lbl = '{} = {:.0f}'.format(data.label, N_data) # dispays data yield
        lbl = '{} '.format(data.label)
        #lbl.SetMarkerSize(1.2)
        #lbl.SetLineWidth(4)
        legend.AddEntry(data_graph, lbl, "ep") #z", ">", and "|>
#    legend.AddEntry(totalmc_SysErr, 'Total SM = {:.1f}'.format(N_sm), "fl") # displays Total SM yields
    legend.AddEntry(totalmc_SysErr, 'Total SM'.format(N_sm), "fl")
    for_sorting = list(sm_inputs)
    if config.sort_stack_by_integral:
        for_sorting = sorted(for_sorting, key=get_integral) # sorts inputs by size
    for sm_ipt in for_sorting[::-1]:
        hist = sm_ipt.stored[central_name]
        legend.AddEntry(hist, sm_ipt.label, "f")
        #Displays background yields
        #Nbla = get_integral(hist, underflow=True)
        #legend.AddEntry(hist, sm_ipt.label + ' = {:.2f}'.format(Nbla), "f")
    for sig_ipt in all_signals:
        hist = sig_ipt.stored[central_name]
        legend.AddEntry(hist, sig_ipt.label, "l")
        #Displays signal yields
        #Nbla = get_integral(hist, underflow=True)
        #legend.AddEntry(hist, sig_ipt.label + ' = {:.5f}'.format(Nbla), "l")

    legend.Draw()

    # adjust_y_range_for_legend(stack, 0.4, hists=for_scaling)
    # Plot the energy, luminosity, ATLAS and region labels
    plot_text(region)


    try:
        # Can be used to rescale the y-axis
        rescale_limits((0.1, 1.75), pad=canvas_1, lo=True)
    except:
        print 'Ohoh, rescaling is not working. Every is probably empty.'

    ##################################
    # The ratio plot
    ##################################
    # Change to the bottom pad
    canvas_2.cd()
    sm_data, ratio_data, add_arrows = ratio_plot(var, data_hist, total_sm, min_max_total)
    sm_data.Draw('a2z')
    if region.blinded is False:
        ratio_data.Draw('p0z same')
    MyLine = ROOT.TLine(sm_data.GetXaxis().GetXmin(),1.0,sm_data.GetXaxis().GetXmax(),1.0)
    MyLine.SetLineWidth(1)
    MyLine.SetLineColor(ROOT.kBlack)
    MyLine.SetLineStyle(1)
    MyLine.Draw()#nope
    for ar in add_arrows:
        ar.Draw()


    ##################################
    # Saving the plot
    ##################################

    for t in config.output_types:
        canvas.Print(outfile.format(t))
        print "Created: ", outfile.format(t)

    if rootfile:
        rootfile.Close()

    helpers.delete_tobject(canvas)
    helpers.delete_tobject(ratio_data)
    helpers.delete_tobject(sm_data)
    helpers.delete_tobject(legend)
    helpers.delete_tobject(data_graph)
    helpers.delete_tobject(totalmc_SysErr)
    helpers.delete_tobject(stack)


def ratio_plot(var, data, total_sm, min_max_sm):
    max_ratio = 2.49
    ctr = []
    wdh = []
    for ibin in range(1, total_sm.GetNbinsX()+1):
        ctr.append(total_sm.GetXaxis().GetBinCenter(ibin))
        wdh.append(total_sm.GetXaxis().GetBinWidth(ibin) / 2.)
    ctr = np.array(ctr)
    wdh = np.array(wdh)
    sm, _ = helpers.convert_hist_to_arrays(total_sm)
    if data is not None:
        dt, _ = helpers.convert_hist_to_arrays(data)
    else:
        dt, _ = helpers.convert_hist_to_arrays(total_sm)
    dt_hi = utils.calcPoissonCLUpper(0.68, dt) - dt
    dt_lo = dt - utils.calcPoissonCLLower(0.68, dt)
    sm_lo, sm_hi = min_max_sm
    # Have to unify the first bin rule. Here, first entry is first bin, not
    # underflow
    sm_lo, sm_hi = sm_lo[1:], sm_hi[1:]

    # Now compute all the ratios
    r_sm_lo = sm_lo / sm
    r_sm_hi = sm_hi / sm
    r_dt_lo = dt_lo / sm
    r_dt_hi = dt_hi / sm
    r_dt = dt / sm
    # Central value of SM by SM ratio is obvisouly a list of ones.
    r_sm = np.ones(len(sm))
    # Numpy index masks to selected broken entries and set them to 0.
    # E.g. r_sm_lo[(sm == 0)] selects all entries in r_sm_lo where sm is 0.
    # Those entries are non-numbers as they are the result of a division by
    # zero. We set all of them to zero.
    r_sm_lo[(sm == 0)] = -0.00000001
    r_sm_hi[(sm == 0)] = -0.00000001
    r_dt_lo[(sm == 0) | (dt == 0)] = -0.00000001
    r_dt_hi[(sm == 0) | (dt == 0)] = -0.00000001
    r_dt[(dt == 0) | (sm == 0)] = -0.00000001

    # Get a list of zeros of matching length for the errorbars in x direction
    zeros = np.zeros(len(dt))

    # Convert the data/SM ratios to a TGraph for plotting
    ratio_data = utils.make_error_graph(ctr, r_dt, zeros,
                                        zeros, r_dt_lo, r_dt_hi)

    # Convert the SM/SM ratios to a Graph to plot the systematic uncertainty
    # band in the background
    sm_data = utils.make_error_graph(ctr, r_sm, wdh,
                                     wdh, r_sm_lo, r_sm_hi)

    sm_data.SetFillStyle(3004)
    sm_data.SetFillColor(1)
    #sm_data.SetFillColor(helpers.get_color(config.ratio_color))
    sm_data.GetXaxis().SetTitle(var.get_full_xlabel())
    sm_data.GetYaxis().SetTitle('Data / SM')
    sm_data.GetYaxis().SetTitleOffset(1.0)
    sm_data.GetYaxis().SetNdivisions(503)
    sm_data.GetXaxis().SetLimits(var.bins[1], var.bins[2])
    sm_data.GetXaxis().SetTitleOffset(4.)
    # Set the value of the range from 0 to 2. Use crude numbers to prevent
    # ROOT from placing an axis label for 0 and 2 at the edges of the y axis
    # which would collide with the top plot.
    # sm_data.GetYaxis().SetRangeUser(0.01, 1.99)
    add_arrows = []
    for c, v in zip(ctr, r_dt-r_dt_lo):
        print c, v
        if v >= max_ratio:
            arrow = ROOT.TArrow(c, 1.7, c, max_ratio)
            arrow.SetLineColor(ROOT.kRed)
            arrow.SetLineWidth(3)
            arrow.SetArrowSize(0.02)
            add_arrows += [arrow]
    sm_data.SetMinimum(0.0)
    sm_data.SetMaximum(max_ratio)
    return sm_data, ratio_data, add_arrows


def make_arrow(position, length=15,height=5, right=True):
    MyCutLine = ROOT.TLine(position,0,position, length)
    MyCutLine.SetLineWidth(3)
    MyCutLine.SetLineColor(ROOT.kBlack)
    MyCutLine.Draw()
    if position is 0:
        MyArrow = ROOT.TArrow(0,0,0,0)
    else:
        if right:
            MyArrow = ROOT.TArrow(position,length,position+height,length)
        else:
            MyArrow = ROOT.TArrow(position,length,position-height,length)
        MyArrow.SetLineWidth(2)
        MyArrow.SetArrowSize(0.02)
        MyArrow.SetLineColor(ROOT.kBlack)
        MyArrow.Draw()
    return MyCutLine, MyArrow

def make_arrow_double(position, length=15,height=5, right=True):
    MyCutLine = ROOT.TLine(position,0,position, length)
    MyCutLine.SetLineWidth(3)
    MyCutLine.SetLineColor(ROOT.kBlack)
    MyCutLine.Draw()
    if position is 0:
        MyArrowRight = ROOT.TArrow(0,0,0,0)
        MyArrowLeft = ROOT.TArrow(0,0,0,0)
    else:
        MyArrowRight = ROOT.TArrow(position,length,position+height,length)
        MyArrowLeft = ROOT.TArrow(position,length,position-height,length)
        MyArrowRight.SetLineWidth(2)
        MyArrowRight.SetArrowSize(0.02)
        MyArrowRight.SetLineColor(ROOT.kBlack)
        MyArrowRight.Draw()
        MyArrowLeft.SetLineWidth(2)
        MyArrowLeft.SetArrowSize(0.02)
        MyArrowLeft.SetLineColor(ROOT.kBlack)
        MyArrowLeft.Draw()
    return MyCutLine, MyArrowRight, MyArrowLeft
