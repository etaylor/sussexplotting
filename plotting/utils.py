from math import sqrt
import math
import logging as log
import ROOT
import numpy as np
from ROOT import TMath


def add_overflow_to_last_bin(hist):
    binLast = hist.GetNbinsX()
    binOverflow = binLast + 1
    hist.SetBinContent(binLast, (hist.GetBinContent(binOverflow) +
                                 hist.GetBinContent(binLast)))
    hist.SetBinError(binLast, sqrt(hist.GetBinError(binOverflow)**2 +
                                   hist.GetBinError(binLast)**2))

    hist.SetBinContent(binOverflow, 0.)
    hist.SetBinError(binOverflow, 0.)


def get_ndc_x(x, pad):
    pad.Update()
    return (x - pad.GetX1())/(pad.GetX2()-pad.GetX1())


def get_ndc_y(y, pad):
    pad.Update()
    return (y - pad.GetY1())/(pad.GetY2()-pad.GetY1())


def make_poisson_error_graph(hist, scale=1.):

    graph = ROOT.TGraphAsymmErrors()
    nbins = hist.GetNbinsX()

    for ibin in range(1, nbins + 1):
        y = hist.GetBinContent(ibin)
        x = hist.GetBinCenter(ibin)

        if y == 0.:
            eyhigh = 0.
        else:
            eyhigh = calcPoissonCLUpper(0.68, y) - y

        eylow = y - calcPoissonCLLower(0.68, y)

        graph.SetPoint((ibin - 1), x, y * scale)
        graph.SetPointError((ibin - 1), 0., 0., eylow * scale, eyhigh * scale)

    return graph


def make_error_graph(x, y, x_lo, x_hi, y_lo, y_hi):

    graph = ROOT.TGraphAsymmErrors()
    for i, (ix, iy, ix_lo, ix_hi, iy_lo, iy_hi) in enumerate(zip(x, y, x_lo, x_hi, y_lo, y_hi)):
        graph.SetPoint(i, ix, iy)
        graph.SetPointError(i, ix_lo, ix_hi, iy_lo, iy_hi)

    return graph

@np.vectorize
def calcPoissonCLLower(q, obs):
    LL = 0.
    if obs >= 0.:
        a = (1. - q) / 2.  # = 0.025 for 95% confidence interval
        LL = TMath.ChisquareQuantile(a, 2. * obs) / 2.
    return LL


@np.vectorize
def calcPoissonCLUpper(q, obs):
    UL = 0.
    if obs >= 0.:
        a = 1. - (1. - q) / 2.  # = 0.025 for 95% confidence interval
        UL = TMath.ChisquareQuantile(a, 2. * (obs + 1.)) / 2.
    return UL


def my_text(x, y, color, text, tsize):
    l = ROOT.TLatex()
    l.SetTextAlign(12)
    l.SetTextSize(tsize)
    l.SetNDC()
    l.SetTextColor(color)
    l.DrawLatex(x, y, text)

def my_atlas_label(x, y):
    l = ROOT.TLatex()
    l.SetNDC()
    l.SetTextFont(72)
    l.SetTextColor(1)
    l.SetTextSize(0.06)
    l.DrawLatex(x, y, "ATLAS")
    l.SetTextFont(42)
    l.DrawLatex(x + 0.10, y, "Internal")
    #l.DrawLatex(x + 0.13, y, "Preliminary")


def my_region_text(region):
    l = ROOT.TLatex()
    l.SetNDC()
    l.SetTextSize(20)
    l.DrawLatex(0.15, 0.65, region.label)


def plot_text(region):
    my_text(0.15, 0.80, 1, "#sqrt{s}= 13 TeV, 139 fb^{-1}", 20)
    my_atlas_label(0.15, 0.86)
    my_region_text(region)



def get_integral(hist, overflow=False, underflow=False):
    """Computes the sum of bin contents. Optionally, overflow can be included.
    Be careful about double counting though."""
    nbins = hist.GetNbinsX()
    fbin = 1
    if underflow:
        fbin = 0
    if overflow:
        nbins += 1
    return hist.Integral(fbin, nbins)

def rescale_limits(scale=(1.0, 1.1), pad=None, lo=False, hi=False):
    r"""
   Rescale the y-axis limits on an axis.

   :param scale: The scale factor(s). This can either be a single number or a
          bi-tuple of numbers / ``None``.

              - If it is a single number, the same scale factor will be
                applied to both the lower and upper limits (albeit in
                different directions).

              - If this is a bi-tuple, the two limits get different scalings,
                the parameter is decomposed as:

                    ``scale_lo, scale_hi = scale``

                A value of ``None`` denotes that this scale factor shall not
                be applied. For linear scale, the scaling procedure works more
                or less like this:

                .. math:

                    delta = y_max - y_min
                    y_up = y_max + (scale - 1.0) * delta
                    y_lo = y_min - (scale - 1.0) * delta

                Hence, for :math:`y_min = 1`, :math:`y_max = 10` the following
                limits are obtained for different inputs:

                    - ``scale = 1.1``: :math:`y_lo = 0.1`, :math:`y_hi = 10.9`
                    - ``scale = 0.9``: :math:`y_lo = 1.9`, :math:`y_hi = 9.1`

                 For a logarithmic scale, the procedure is applied to the
                 effective axis length.

   :param pad: The ROOT :cernroot:`TPad` onto which the procedure is applied,
          defaults to the global pad.

   :param bool lo: If ``True`` force the the lower axis limit to be
          ``scale_lo``.
   :param bool hi: If ``True`` force the the upper axis limit to be
          ``scale_hi``.
   """
    pad = pad or ROOT.gPad
    if not pad:
        raise ReferenceError("invalid canvas / pad reference")

    if isinstance(scale, (float, int, long)):
        sf_lo = scale
        sf_hi = scale
    else:
        sf_lo, sf_hi = scale

    if (sf_lo and sf_lo <= 0) or (sf_hi and sf_hi <= 0):
        raise ValueError("scale must be > 0")

    if pad.GetListOfPrimitives().GetSize() < 1:
        raise RuntimeError(
            "list of primitives must contain at least one elements")

    _first = pad.GetListOfPrimitives()[0]
    if issubclass(type(_first), (ROOT.TH1, ROOT.THStack, ROOT.TGraph)):
        axes = _first

    elif not isinstance(_first, ROOT.TFrame):
        raise RuntimeError(
            "first element in list of primitives is not a TFrame: '%s'" %
            (type(_first).__name__))

    else:
        # Retrieve the top-level plot (the one responsible for the axes).
        axes = pad.GetListOfPrimitives()[1]
        if not issubclass(type(axes), (ROOT.TH1, ROOT.THStack, ROOT.TGraph)):
            raise RuntimeError(
                "second element in list of primitives does not match expected type: '%s'" %
                (type(axes).__name__))

    del _first

    # Find minimum and maximum
    min_val = float('+inf')
    max_val = float('-inf')

    for p in pad.GetListOfPrimitives():
        if issubclass(type(p), ROOT.THStack):
            stack = p.GetStack()
            min_val = min(stack[0].GetMinimum(), min_val)
            max_val = max(stack[p.GetNhists() - 1].GetMaximum(), max_val)
            if min_val == 0:
                min_val = 10**(ROOT.gPad.GetY1())
        elif issubclass(type(p), ROOT.TGraph):
            min_val = min(ROOT.TMath.MinElement(p.GetN(), p.GetY()), min_val)
            max_val = max(ROOT.TMath.MaxElement(p.GetN(), p.GetY()), max_val)
        else:
            try:
                min_val = min(p.GetMinimum(), min_val)
                max_val = max(p.GetMaximum(), max_val)
            except AttributeError:
                pass

    if min_val <= 0 and pad.GetLogy():
        min_val = 10**(ROOT.gPad.GetY1())

    assert (min_val != float('+inf') and max_val != float('-inf')), \
        "failure to determine y-axis value range"

    log.debug("data limits: [%f, %f]" % (min_val, max_val))

    delta = (sf_hi if hi else max_val) - (sf_lo if lo else min_val)
    assert delta >= 0, "sanity check failed"

    if pad.GetLogy():
        hi_dec = math.ceil(math.log10(sf_hi if hi else max_val))
        lo_dec = math.floor(math.log10(sf_lo if lo else min_val))
        n_dec = int(hi_dec - lo_dec)

    if sf_lo is not None:
        if pad.GetLogy():
            lim_lo = min_val / 10 ** (n_dec * (sf_lo - 1.0))

        else:
            lim_lo = min_val - (sf_lo - 1.0) * delta
    else:
        lim_lo = min_val

    if sf_hi is not None:
        if pad.GetLogy():
            lim_hi = max_val * 10 ** (n_dec * (sf_hi - 1.0))

        else:
            lim_hi = max_val + (sf_hi - 1.0) * delta
    else:
        lim_hi = max_val

    if lo:
        lim_lo = sf_lo
    if hi:
        lim_hi = sf_hi

    if pad.GetLogy():
        if lim_lo == 0:
            if lo:
                log.error(
                    "Can not force lo to 0 for logarithmic axis, will use minimum value")

            log.debug("reverting lim_lo to (almost) min_val")
            lim_lo = min_val + delta * 1e-12

        if lim_hi == 0:
            if lo:
                log.error(
                    "Can not force hi to 0 for logarithmic axis, will use maximum value")

            log.debug("reverting lim_hi to (almost) max_val")
            lim_hi = max_val - delta * 1e-12

    if issubclass(type(axes), ROOT.THStack):
        axes.SetMinimum(lim_lo)
        axes.SetMaximum(lim_hi)
    else:
        axes.GetYaxis().SetRangeUser(lim_lo, lim_hi)

    pad.Update()
    pad.Modified()
    pad.Update()
