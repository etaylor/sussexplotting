#ifndef K3PI_CPP
#define K3PI_CPP

#include <map>
#include <string>
#include <vector>

void TreeSplitter(std::vector<std::string> files,
                  std::vector<std::string> variables, std::string output,
                  std::string treename, std::string outputtreename = "",
                  std::string selection = "1",
                  std::map<std::string, std::string> addvariables =
                      std::map<std::string, std::string>());

#endif /* ifndef k3pi_cpp */
